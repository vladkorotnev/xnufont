//
//  main.m
//  mkxfimg
//
//  Created by Akasaka Ryuunosuke on 05/01/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../../isofont/XNUFontRenderer.h"
int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        
        if (argc < 5) {
            printf("mkxfimg by Akasaka, 2015\n");
            printf("Usage: %s <textfile> <width> <height> <pngfile>\n", argv[0]);
            printf("Please note anything outside 255 ANSI symbols is NOT supported.\n");
            return 2;
        }
        NSError *e;
        NSString *input = [NSString stringWithContentsOfFile:[NSString stringWithCString:argv[1] encoding:NSUTF8StringEncoding] encoding:NSUTF8StringEncoding error:&e];
        if (e) {
            printf("Error loading text file. %s\n",[e.localizedDescription cStringUsingEncoding:NSUTF8StringEncoding]);
            return 1;
        }
        
        NSInteger width = atoi(argv[2]); NSInteger height = atoi(argv[3]);
        
        NSImage *img = [XNUFontRenderer renderString:input inSize:CGSizeMake(width, height) wrapping:TRUE];
        [img lockFocus] ;
        NSBitmapImageRep *imgRep = [[NSBitmapImageRep alloc] initWithFocusedViewRect:NSMakeRect(0.0, 0.0, [img size].width, [img size].height)] ;
        [img unlockFocus] ;
        NSData *data = [imgRep representationUsingType: NSPNGFileType properties: nil];
        [data writeToFile: [NSString stringWithCString:argv[4] encoding:NSUTF8StringEncoding] atomically: NO];
    }
    return 0;
}
