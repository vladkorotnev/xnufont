//
//  XNUFontRenderer.h
//  isofont
//
//  Created by Akasaka Ryuunosuke on 05/01/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>
@interface XNUFontRenderer : NSObject
+ (NSImage*) renderCharacter: (unsigned char) ch;
+ (NSImage*) renderString:(NSString*)string inSize:(CGSize)size wrapping:(BOOL)wrap;
+ (NSImage*) renderCharacter: (unsigned char) ch inverse:(bool)inverse;
+ (NSImage*) renderCharacter: (unsigned char) ch inverse:(bool)inverse funky: (bool)funky;
@end
