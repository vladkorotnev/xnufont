//
//  AppDelegate.m
//  isofont
//
//  Created by Akasaka Ryuunosuke on 05/01/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import "AppDelegate.h"
#import "XNUFontRenderer.h"
@interface AppDelegate ()
            
@property (weak) IBOutlet NSWindow *window;


@end

@implementation AppDelegate
static NSString *msg;
- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    [self.window setBackgroundColor:[NSColor blackColor]];
    [self.window setDelegate:self];
    
    msg = @"Hello world.\nAll \3SAS\2 oelutz.\nMore lines.\nI wonder how it would look like when the text is a bit longer than expected per line.\nTry resizing this or full-screening this.\n\nBit-writing fonts is fun.\n\n\1-= [ Akasaka's XNUFontRenderer ] =-\0\nFeel free to use in your apps.\nMake a sci-fi game with this as a texture generator. Give critical error messages a nice touch. I donno.\nThe \1inverse\0 text thing can be done by placing a \\1\1 where it's supposed to begin and a \\0\0 where it's supposed to end.\nHow about trying to add a \\3\3 to get this funky text effect and a \\2\2 to turn it off?\n\nThis Pangram contains four a's, one b, two c's, one d, thirty e's, six f's, five g's, seven h's, eleven i's, one j, one k, two l's, two m's, eighteen n's, fifteen o's, two p's, one q, five r's, twenty-seven s's, eighteen t's, two u's, seven v's, eight w's, two x's, three y's, & one z.\n\nThe quick brown fox jumps over a lazy dog.\nPack my box with five dozen liquor jugs.\nSphinx of black quartz, judge my vow.\nSqudgy fez, blank jimp crwth vox!\n\nSpecial symbols:!@#$%^&*()_-+[]{}\\|/?><,.';:\"`~\n\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur at ligula arcu. Etiam auctor commodo lorem, et lacinia ante fermentum eu. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Sed non dolor vestibulum lectus bibendum aliquam maximus ac tortor. Pellentesque in urna velit. Etiam at nibh quam. Vestibulum vestibulum orci sodales odio aliquam pretium. Suspendisse potenti. Integer non enim ut massa pulvinar malesuada quis vel nunc. Curabitur quis molestie ipsum. Morbi tortor metus, lobortis non rutrum porta, placerat a magna. Pellentesque nibh justo, egestas ac neque ut, varius molestie urna. Nullam laoreet felis id nibh blandit convallis id in diam.\n\n\1\3~chiisana hoshi no yume~\0\2\n\n";
    for (unsigned char i = 4; i < 255; i++) {
        msg = [msg stringByAppendingString:[NSString stringWithFormat:@"%c",i]];
        NSLog(@"%i",i);
    }
    
    [self render];
}
- (void) render {
    
    [self.iv setImage:[XNUFontRenderer renderString:msg inSize:self.iv.frame.size wrapping:YES]];
}
- (void)windowDidResize:(NSNotification *)notification {
    [self render];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
