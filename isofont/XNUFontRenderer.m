//
//  XNUFontRenderer.m
//  isofont
//
//  Created by Akasaka Ryuunosuke on 05/01/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import "XNUFontRenderer.h"
#import "iso_font.c"
@implementation XNUFontRenderer
unsigned char rev(unsigned char input) {
    unsigned char output = 0, bit;
    for(int count=1;count<=8;count++)
    { bit= input & 0x01;
        input=input>>1;
        output=output<<1;
        if(bit==1)
            output=output+1;
    }
    return output;
}
+ (NSImage*) renderCharacter: (unsigned char) ch {
    return [XNUFontRenderer renderCharacter:ch inverse:false];
}
+ (NSImage*) renderCharacter: (unsigned char) ch inverse:(bool)inverse {
    return [XNUFontRenderer renderCharacter:ch inverse:inverse funky:false];
}
+ (NSImage*) renderCharacter: (unsigned char) ch inverse:(bool)inverse funky: (bool)funky {
    
    NSBitmapImageRep *bmp = [[NSBitmapImageRep alloc]initWithBitmapDataPlanes:NULL
                                                                   pixelsWide:ISO_CHAR_WIDTH
                                                                   pixelsHigh:ISO_CHAR_HEIGHT
                                                                bitsPerSample:1
                                                              samplesPerPixel:1
                                                                     hasAlpha:NO
                                                                     isPlanar:NO
                                                               colorSpaceName:NSDeviceWhiteColorSpace
                                                                 bitmapFormat:0
                                                                  bytesPerRow:1
                                                                 bitsPerPixel:1];
    unsigned char *data = [bmp bitmapData];
    unsigned char *theChar = iso_font + (ch * ISO_CHAR_HEIGHT);
    for (unsigned int y = 0; y < ISO_CHAR_HEIGHT; y+=1) {
        *data = (funky ? (inverse ? ~(theChar[y]) : (theChar[y])) : (inverse ? ~rev(theChar[y]) : rev(theChar[y])));
        data+=1;
    }
    
    NSImage*otp = [[NSImage alloc]initWithCGImage:[bmp CGImage] size:CGSizeMake(ISO_CHAR_WIDTH, ISO_CHAR_HEIGHT)];
    
    return otp;
}
+ (NSImage*) renderString:(NSString*)string inSize:(CGSize)size wrapping:(BOOL)wrap {
    NSImage * otp = [[NSImage alloc]initWithSize:size];
    [otp lockFocus];
    
    int curX = 0;
    int curY = ISO_CHAR_HEIGHT;
    bool curI = false;
    bool curF = false;
    for (int i = 0; i<string.length; i++) {
        char ch = [string characterAtIndex:i];
        if(ch == '\n') {
            curX = 0;
            curY += ISO_CHAR_HEIGHT;
        } else if (ch == '\0') {
            curI = false;
        } else if (ch == '\1') {
            curI = true;
        } else if (ch == '\2') {
            curF = false;
        } else if (ch == '\3') {
            curF = true;
        }
        else {
          [[XNUFontRenderer renderCharacter:ch inverse:curI funky:curF]drawAtPoint:CGPointMake(curX, size.height-curY) fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
            curX += ISO_CHAR_WIDTH;
        }
        
        if (wrap) {
            if (curX > size.width-ISO_CHAR_WIDTH) {
                curX = 0;
                curY += ISO_CHAR_HEIGHT;
            }
        }
    }
    
    [otp unlockFocus];
    return otp;
}
@end
