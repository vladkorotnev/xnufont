//
//  AppDelegate.h
//  isofont
//
//  Created by Akasaka Ryuunosuke on 05/01/15.
//  Copyright (c) 2015 Akasaka Ryuunosuke. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (weak) IBOutlet NSImageView *iv;

@end

